#!/bin/bash

docker stop mytest
docker rm mytest
docker build -t mytest:0.0.1 .
#docker run --name mytest -it mytest:0.0.1
docker run --name mytest -d -p 8084:80 mytest:0.0.1
docker start mytest