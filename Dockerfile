FROM ubuntu:19.10
ENV TZ=Europe/Kiev
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get update
RUN apt-get -y install mc apache2 mariadb-server mariadb-client php php-mysql php-mbstring php-gettext
COPY ./startup.sh /startup.sh
COPY ./initdb.sql /initdb.sql
CMD ["/startup.sh"]